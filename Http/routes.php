<?php

Route::group(['middleware' => ['web','cors'], 'prefix' => 'aytyprovider', 'namespace' => 'Modules\AytyProvider\Http\Controllers'], function()
{
    Route::post('/lead-new','AytyController@newLead');
    Route::post('/lead-update','AytyController@updateLead');
    Route::post('/titular-new','AytyController@newTitular');
    Route::post('/titular-update','AytyController@updateTitular');
    Route::post('/beneficiario-new','AytyController@newBeneficiario');
    Route::post('/beneficiario-update','AytyController@updateBeneficiario');
    Route::get('/check-lead/{lead_id}','AytyController@checkLead');
    Route::get('/generate-cob/{lead_id}','AytyController@generateCob');
    Route::get('/generate-cob2/{lead_id}','AytyController@generateCob2');
    //
});
