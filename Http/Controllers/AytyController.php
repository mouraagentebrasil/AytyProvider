<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 13/02/2017
 * Time: 14:41
 */

namespace Modules\AytyProvider\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AytyProvider\Entities\AytyLogs;
use Modules\Client\Entities\Lead;
use Modules\Client\Entities\LeadAgregado;
use Modules\Client\Entities\LeadCard;
use Modules\Client\Events\EventAlterLeadAgregado;
use Modules\Client\Events\EventRegisterLeadAgregado;
use GuzzleHttp\Exception\ClientException;


class AytyController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function checkLead($lead_id){
        $lead = Lead::where('lead_id',$lead_id)->where('lead_legado_id',$lead_id)->first();
        if(!is_null($lead)){
            return response()->json(['lead_id'=>$lead_id,'legado'=>true],200);
        }
        return response()->json(['lead_id'=>$lead_id,'legado'=>false],200);
    }

    public function newLead(Request $request){
        $data = $request->all();
        $pid = AytyLogs::codProcess();
        try {
            DB::beginTransaction();
            $lead_card = [
                'lead_nome_cartao',
                'lead_num_cartao',
                'lead_mes_cartao',
                'lead_ano_cartao',
                'lead_codigo_cartao',
                'lead_bandeira_cartao'
            ];
            $fields_lc = 0; //numero de campos de cartao
            $field_lcl = array(); //array de campos de cartao
            foreach ($lead_card as $lc) {
                if (array_key_exists($lc, $data) && $data[$lc] != "") {
                    $field_lcl[$lc] = $data[$lc];
                    $fields_lc++;
                }
            }

            $lead_contratacao = [
                'lead_pagamento_forma',
                'come_id',
                'plan_id',
                'lead_vencimento_dia',
                'lead_recorrencia_dia',
                'lead_bene_quant',
                'lead_cobrado',
                'lead_fone2',
                'lead_status',
                'lead_quantidade'
            ];

            $fields_lct = 0; //numero de campos de contratacao
            $field_lctf = []; //array de campos de contratacao
            foreach ($lead_contratacao as $lc) {
                if (array_key_exists($lc, $data)) {
                    $field_lctf[$lc] = $data[$lc];
                    $fields_lct++;
                }
            }
            $data['lead_cep'] = $data['lead_cep_local'];
            unset($data['lead_cep_local']);
            $data['lead_has_exported'] = $data['lead_origem'];
            unset($data['lead_origem']);
            $data['lead_estado_civil'] = $data['lead_civil'];
            unset($data['lead_civil']);

            $diffkey = function ($arrayI, $arrayB) {
                foreach ($arrayI as $k => $v) {
                    if (in_array($k, $arrayB)) {
                        unset($arrayI[$k]);
                    }
                }
                return $arrayI;
            };
            $obj_lead = $diffkey($data, $lead_card);
            $obj_lead = $diffkey($obj_lead, $lead_contratacao);
            $obj_lead['lead_object_ayty'] = json_encode($field_lctf);
            $obj_lead['lead_has_exported'] = 'aytyab';
            $obj_lead['lead_criacao'] = date('Y-m-d H:i:s');
            $lead_id = Lead::create($obj_lead)->lead_id;
            $data['lead_id'] =  $lead_id;
            if (count($lead_card) == $fields_lc) {
                $save_card = array();
                $save_card['lead_id'] = $lead_id;
                //
                $save_card['lead_card_titular_nome'] = $data['lead_nome_cartao'];
                $save_card['lead_card_numero'] = $data['lead_num_cartao'];
                $save_card['lead_card_mes'] = $data['lead_mes_cartao'];
                $save_card['lead_card_ano'] = $data['lead_ano_cartao'];
                $save_card['lead_card_cvv'] = $data['lead_codigo_cartao'];
                $save_card['lead_card_bandeira'] = $data['lead_bandeira_cartao'];
                //
                LeadCard::create($save_card);
            }

            $objOk = ['return' => 200, 'id' => $lead_id, 'pid' => $pid, 'action' => 'inserido lead'];
            $data['lead_dddtelefone'] = $data['lead_fone'];
            DB::commit();
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserir lead',
                AytyLogs::formataLinhaLeadAytyLog($data), json_encode($objOk), $data['lead_id'], '0.0.0.0'
            );
            return response()->json(['return' => 200, 'id' => $lead_id, 'pid' => $pid, 'action' => 'inserido lead'], 200);
        } catch (\Exception $e){
            DB::rollback();
            $data['lead_dddtelefone'] = $data['lead_fone'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserir lead',
                AytyLogs::formataLinhaLeadAytyLog($data), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), null, '0.0.0.0'
            );

            return response()->json(['return' => 500, 'id' => null, 'pid' => $pid, 'action' => 'inserido lead'], 200);
        }
    }

    public function updateLead(Request $request){
        $data = $request->all();

        $pid = AytyLogs::codProcess();
        try {
            //DB::beginTransaction();
            $lead_card = [
                'lead_nome_cartao',
                'lead_num_cartao',
                'lead_mes_cartao',
                'lead_ano_cartao',
                'lead_codigo_cartao',
                'lead_bandeira_cartao'
            ];
            $fields_lc = 0; //numero de campos de cartao
            $field_lcl = array(); //array de campos de cartao
            foreach ($lead_card as $lc) {
                if (array_key_exists($lc, $data) && $data[$lc] != "") {
                    $field_lcl[$lc] = $data[$lc];
                    $fields_lc++;
                }
            }

            $lead_contratacao = [
                'lead_pagamento_forma',
                'come_id',
                'plan_id',
                'lead_vencimento_dia',
                'lead_recorrencia_dia',
                'lead_bene_quant',
                'lead_cobrado',
                'lead_fone2',
                'lead_status',
                'lead_quantidade'
            ];

            $fields_lct = 0; //numero de campos de contratacao
            $field_lctf = []; //array de campos de contratacao
            foreach ($lead_contratacao as $lc) {
                if (array_key_exists($lc, $data)) {
                    $field_lctf[$lc] = $data[$lc];
                    $fields_lct++;
                }
            }

            $data['lead_sexo'] = $data['lead_sexo'] == "masculino" ? "m" : "f";
            $data['lead_cep'] = $data['lead_cep_local'];
            unset($data['lead_cep_local']);
            $data['lead_has_exported'] = $data['lead_origem'];
            unset($data['lead_origem']);
            $data['lead_estado_civil'] = $data['lead_civil'];
            unset($data['lead_civil']);

            $diffkey = function ($arrayI, $arrayB) {
                foreach ($arrayI as $k => $v) {
                    if (in_array($k, $arrayB)) {
                        unset($arrayI[$k]);
                    }
                }
                return $arrayI;
            };
            $obj_lead = $diffkey($data, $lead_card);
            $obj_lead = $diffkey($obj_lead, $lead_contratacao);
            $obj_lead['lead_object_ayty'] = json_encode($field_lctf);
            $obj_lead['lead_has_exported'] = 'aytyab';
            $obj_lead['lead_criacao'] = date('Y-m-d H:i:s');
            $obj_lead['lead_cobrado'] = $data['lead_cobrado'];

            Lead::where('lead_id',$data['lead_id'])->update($obj_lead);
            $lead_id = $data['lead_id'];
            if (count($lead_card) == $fields_lc) {
                $save_card = array();
                $save_card['lead_id'] = $lead_id;
                //
                $save_card['lead_card_titular_nome'] = $data['lead_nome_cartao'];
                $save_card['lead_card_numero'] = $data['lead_num_cartao'];
                $save_card['lead_card_mes'] = $data['lead_mes_cartao'];
                $save_card['lead_card_ano'] = $data['lead_ano_cartao'];
                $save_card['lead_card_cvv'] = $data['lead_codigo_cartao'];
                $save_card['lead_card_bandeira'] = $data['lead_bandeira_cartao'];
                //
                LeadCard::create($save_card);
            }
            $post = [
                'titu_nome'=>$data['lead_nome'],
                'titu_cpf'=>$data['lead_cpf'],
                'titu_nascimento'=>$data['lead_nascimento'],
                'titu_mae'=>$data['lead_mae'],
                'titu_sexo'=>$data['lead_sexo'] =="masculino" ? "m" : "f",
                'lead_id'=>$data['lead_id'],
                'titu_parentesco'=>'proprio',
                'titu_civil'=>$data['lead_estado_civil'],
                'titu_tipo'=>$data['lead_cobrado']
            ];
            $saveTitular = LeadAgregado::where('lead_agregado_cpf',$data['lead_cpf'])
                ->where('lead_id',$data['lead_id'])
                ->first();
            if($data['lead_cobrado']=="titular_pagador" && is_null($saveTitular) ) {

                $req = $this->client->request('POST','https://client.agentebrasil.com/aytyprovider/titular-new',[
                    'verify'=>false,
                    'form_params'=>$post
                ]);
                $return = json_decode($req->getBody()->getContents());
            }
            $objOk = ['return' => 200, 'id' => $lead_id, 'pid' => $pid, 'action' => 'alterado lead'];
            $data['lead_dddtelefone'] = $data['lead_fone'];
            //DB::commit();
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'alterado lead',
                AytyLogs::formataLinhaLeadAytyLog($data), json_encode($objOk), $data['lead_id'], '0.0.0.0'
            );
            return response()->json(['return' => 200, 'id' => $lead_id, 'pid' => $pid, 'action' => 'alterado lead'], 200);
        }
        catch (\Exception $e){
            //DB::rollback();
            $data['lead_dddtelefone'] = $data['lead_fone'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'alterado lead',
                AytyLogs::formataLinhaLeadAytyLog($data), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), null, '0.0.0.0'
            );

            return response()->json(['return' => 500, 'id' => null, 'pid' => $pid, 'action' => 'alterado lead'], 200);
        }
    }

    public function newTitular(Request $request)
    {
        $pid = AytyLogs::codProcess();
        $data = $request->all();
        try {
            //DB::beginTransaction();
            $lead = Lead::where('lead_id',$data['lead_id'])->first();
            $dataLA = [
                'lead_agregado_tipo'=>isset($data['titu_tipo']) || empty($data['titu_tipo']) ? "titular" : "titular_pagador",
                'lead_agregado_nome'=>$data['titu_nome'],
                'lead_agregado_cpf'=>$data['titu_cpf'],
                'lead_agregado_nascimento'=>$data['titu_nascimento'],
                'lead_agregado_mae'=>$data['titu_mae'],
                'lead_agregado_email'=>$lead->lead_email,
                'lead_agregado_fone'=>$lead->lead_fone,
                'lead_agregado_cep'=>$lead->lead_cep,
                'lead_agregado_uf'=>$lead->lead_uf,
                'lead_agregado_cidade'=>$lead->lead_cidade,
                'lead_agregado_bairro'=>$lead->lead_bairro,
                'lead_agregado_logradouro'=>$lead->lead_logradouro,
                'lead_agregado_complemento'=>$lead->lead_complemento,
                'lead_agregado_numero'=>$lead->lead_numero,
                'lead_agregado_sexo'=>$data['titu_sexo'] == "masculino" ? "m" : "f",
                'lead_agregado_criacao'=>date('Y-m-d H:i:s'),
                'fl_ativo'=>1,
                'lead_id'=>$data['lead_id'],
                'lead_agregado_parentesco'=>$data['titu_parentesco'],
                'lead_agregado_civil'=>$data['titu_civil'],
                //'lead_agregado_meta_id'
            ];
            $la_id = LeadAgregado::create($dataLA)->lead_agregado_id;
            LeadAgregado::where('lead_agregado_id',$la_id)->update(['lead_agregado_meta_id'=>$la_id]);
            //
            Lead::where('lead_id',$data['lead_id'])->increment('lead_ayty_num_call');
            $findLead = Lead::where('lead_id',$data['lead_id'])->first();
            $object_ayty = json_decode($findLead->lead_object_ayty);
            if($object_ayty->lead_quantidade == $findLead->lead_ayty_num_call && $findLead->lead_ayty_num_call_finalizado==0){
                $req = $this->client->request('GET','http://client.agentebrasil.com/aytyprovider/generate-cob/'.$data['lead_id']);
                $return = json_decode($req->getBody()->getContents());
                if($return->return == 200){
                    Lead::where('lead_id',$data['lead_id'])->update(['lead_ayty_num_call_finalizado'=>1]);
                }
            }
            //
            event(new EventRegisterLeadAgregado($data));
            //DB::commit();
            $objOK = ['return'=>'200','id'=>$la_id,'pid'=>$pid,'action'=>'inserido titular'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserido titular',
                AytyLogs::formataLinhaLeadAgregadoAytyLog($data), json_encode($objOK), $data['lead_id'], '0.0.0.0'
            );
            return response()->json($objOK,200);
        } catch (\Exception $e){
            DB::rollback();
            $objOK = ['return'=>'500','id'=>null,'pid'=>$pid,'action'=>'inserido titular'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserido titular',
                AytyLogs::formataLinhaLeadAgregadoAytyLog($data), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), $request->input('lead_id'), '0.0.0.0'
            );
            return response()->json($objOK,200);
        }
    }

    public function updateTitular(Request $request)
    {
        $pid = AytyLogs::codProcess();
        $data = $request->all();
        try {
            DB::beginTransaction();
            $dataOld = LeadAgregado::find($data['titu_id'])->toArray();

            $lead = Lead::where('lead_id',$data['lead_id'])->first();
            $dataLA = [
                'lead_agregado_tipo'=>'titular',
                'lead_agregado_nome'=>$data['titu_nome'],
                'lead_agregado_cpf'=>$data['titu_cpf'],
                'lead_agregado_nascimento'=>$data['titu_nascimento'],
                'lead_agregado_mae'=>$data['titu_mae'],
                'lead_agregado_email'=>$lead->lead_emai,
                'lead_agregado_fone'=>$lead->lead_fone,
                'lead_agregado_cep'=>$lead->lead_cep,
                'lead_agregado_uf'=>$lead->lead_uf,
                'lead_agregado_cidade'=>$lead->lead_cidade,
                'lead_agregado_bairro'=>$lead->lead_bairro,
                'lead_agregado_logradouro'=>$lead->lead_logradouro,
                'lead_agregado_complemento'=>$lead->lead_complemento,
                'lead_agregado_numero'=>$lead->lead_numero,
                'lead_agregado_sexo'=>$data['titu_sexo'],
                'lead_agregado_criacao'=>date('Y-m-d H:i:s'),
                'fl_ativo'=>1,
                'lead_id'=>$data['lead_id'],
                'lead_agregado_parentesco'=>$data['titu_parentesco'],
                'lead_agregado_civil'=>$data['titu_civil'],
                //'lead_agregado_meta_id'
            ];
            LeadAgregado::where('lead_agregado_meta_id',$data['titu_id'])
                ->whereIn('lead_agregado_tipo',['titular','titular_pagador'])
                ->update($dataLA);
            $dataLA['usuario_id'] = 191; //AYTY
            event(new EventAlterLeadAgregado($dataOld,$dataLA));
            DB::commit();
            $objOK = ['return'=>'200','id'=>$data['titu_id'],'pid'=>$pid,'action'=>'alterado titular'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserido titular',
                AytyLogs::formataLinhaLeadAgregadoAytyLog($data), json_encode($objOK), $data['lead_id'], '0.0.0.0'
            );
            return response()->json($objOK,200);
        } catch (\Exception $e){
            DB::rollback();
            $objOK = ['return'=>'500','id'=>null,'pid'=>$pid,'action'=>'alterado titular'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserido titular',
                AytyLogs::formataLinhaLeadAgregadoAytyLog($data), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), $request->input('lead_id'), '0.0.0.0'
            );
            return response()->json($objOK,200);
        }
    } //ok

    public function newBeneficiario(Request $request)
    {
        $pid = AytyLogs::codProcess();
        $data = $request->all();
        try {
            //DB::beginTransaction();
            $lead = Lead::where('lead_id',$data['lead_id'])->first();
            $dataLA = [
                'lead_agregado_tipo'=>'beneficiario',
                'lead_agregado_nome'=>$data['bene_nome'],
                'lead_agregado_cpf'=>$data['bene_cpf'],
                'lead_agregado_nascimento'=>$data['bene_nascimento'],
                'lead_agregado_mae'=>$data['bene_mae'],
                'lead_agregado_email'=>$lead->lead_emai,
                'lead_agregado_fone'=>$lead->lead_fone,
                'lead_agregado_cep'=>$lead->lead_cep,
                'lead_agregado_uf'=>$lead->lead_uf,
                'lead_agregado_cidade'=>$lead->lead_cidade,
                'lead_agregado_bairro'=>$lead->lead_bairro,
                'lead_agregado_logradouro'=>$lead->lead_logradouro,
                'lead_agregado_complemento'=>$lead->lead_complemento,
                'lead_agregado_numero'=>$lead->lead_numero,
                'lead_agregado_sexo'=>$data['bene_sexo']=="masculino" ? "m" : "f",
                'lead_agregado_criacao'=>date('Y-m-d H:i:s'),
                'fl_ativo'=>1,
                'lead_id'=>$data['lead_id'],
                'lead_agregado_parentesco'=>$data['bene_parentesco'],
                'lead_agregado_civil'=>$data['bene_civil'],
                //'lead_agregado_meta_id'
            ];
            $la_id = LeadAgregado::create($dataLA)->lead_agregado_id;
            LeadAgregado::where('lead_agregado_id',$la_id)->update(['lead_agregado_meta_id'=>$la_id]);
            //
            Lead::where('lead_id',$data['lead_id'])->increment('lead_ayty_num_call');
            $findLead = Lead::where('lead_id',$data['lead_id'])->first();
            $object_ayty = json_decode($findLead->lead_object_ayty);
            if($object_ayty->lead_quantidade == $findLead->lead_ayty_num_call && $findLead->lead_ayty_num_call_finalizado==0){
                $req = $this->client->request('GET','http://client.agentebrasil.com/aytyprovider/generate-cob/'.$data['lead_id'],['verfify'=>false]);
                $return = json_decode($req->getBody()->getContents());
                if($return->return == 200){
                    Lead::where('lead_id',$data['lead_id'])->update(['lead_ayty_num_call_finalizado'=>1]);
                }
            }
            //
            event(new EventRegisterLeadAgregado($data));
            //DB::commit();
            $objOK = ['return'=>'200','id'=>$la_id,'pid'=>$pid,'action'=>'inserido beneficiario'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserido beneficiario',
                AytyLogs::formataLinhaLeadAgregadoBeneficiarioAytyLog($data), json_encode($objOK), $data['lead_id'], '0.0.0.0'
            );
            return response()->json($objOK,200);
        } catch (\Exception $e){
            //DB::rollback();
            $objOK = ['return'=>'500','id'=>null,'pid'=>$pid,'action'=>'inserido beneficiario'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserido beneficiario',
                AytyLogs::formataLinhaLeadAgregadoBeneficiarioAytyLog($data), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), $request->input('lead_id'), '0.0.0.0'
            );
            return response()->json($objOK,200);
        }
    } //ok

    public function updateBeneficiario(Request $request)
    {
        $pid = AytyLogs::codProcess();
        $data = $request->all();
        try {
            DB::beginTransaction();
            $dataOld = LeadAgregado::find($data['bene_id'])->toArray();
            $lead = Lead::where('lead_id',$data['lead_id'])->first();
            $dataLA = [
                'lead_agregado_tipo'=>'beneficiario',
                'lead_agregado_nome'=>$data['bene_nome'],
                'lead_agregado_cpf'=>$data['bene_cpf'],
                'lead_agregado_nascimento'=>$data['bene_nascimento'],
                'lead_agregado_mae'=>$data['bene_mae'],
                'lead_agregado_email'=>$lead->lead_emai,
                'lead_agregado_fone'=>$lead->lead_fone,
                'lead_agregado_cep'=>$lead->lead_cep,
                'lead_agregado_uf'=>$lead->lead_uf,
                'lead_agregado_cidade'=>$lead->lead_cidade,
                'lead_agregado_bairro'=>$lead->lead_bairro,
                'lead_agregado_logradouro'=>$lead->lead_logradouro,
                'lead_agregado_complemento'=>$lead->lead_complemento,
                'lead_agregado_numero'=>$lead->lead_numero,
                'lead_agregado_sexo'=>$data['bene_sexo'],
                'lead_agregado_criacao'=>date('Y-m-d H:i:s'),
                'fl_ativo'=>1,
                'lead_id'=>$data['lead_id'],
                'lead_agregado_parentesco'=>$data['bene_parentesco'],
                'lead_agregado_civil'=>$data['bene_civil'],
                //'lead_agregado_meta_id'
            ];
            LeadAgregado::where('lead_agregado_meta_id',$data['bene_id'])
                ->where('lead_agregado_tipo','beneficiario')
                ->update($dataLA);
            $dataLA['usuario_id'] = 191; //AYTY
            event(new EventAlterLeadAgregado($dataOld,$dataLA));
            DB::commit();
            $objOK = ['return'=>'200','id'=>$data['bene_id'],'pid'=>$pid,'action'=>'alterado titular'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'alterado beneficiario',
                AytyLogs::formataLinhaLeadAgregadoBeneficiarioAytyLog($data), json_encode($objOK), $data['lead_id'], '0.0.0.0'
            );
            return response()->json($objOK,200);
        } catch (\Exception $e){
            DB::rollback();
            $objOK = ['return'=>'500','id'=>null,'pid'=>$pid,'action'=>'alterado beneficiario'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'inserido titular',
                AytyLogs::formataLinhaLeadAgregadoBeneficiarioAytyLog($data), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), $request->input('lead_id'), '0.0.0.0'
            );
            return response()->json($objOK,200);
        }
    }

    public function generateCob($lead_id){
        $pid = AytyLogs::codProcess();
        $objLead = Lead::with('LeadAgregado')->where('lead_id',$lead_id)->get()->toArray();
        if(empty($objLead)) return ['return'=>500,'pid'=>$pid,'id'=>$lead_id,'action'=>'cobranca'];
        $lead_agregado_id = [];
        foreach ($objLead[0]['lead_agregado'] as $la){
            $lead_agregado_id[] = $la['lead_agregado_id'];
        }
        $object = json_decode($objLead[0]['lead_object_ayty']);
        $data = [
            'financeiro_contratacao'=>[
                'data_vencimento'=>$object->lead_vencimento_dia,
                'dia_recorrencia'=>$object->lead_recorrencia_dia,
                'financeiro_forma_pagamento_id'=>$object->lead_pagamento_forma == "boleto" ? 1 : 2
            ],
            'lead_agregado_produto'=>[
                'produto_id'=>$object->plan_id,
                'usuario_id'=>$object->come_id,
            ],
            'lead_agregado_produto_itens'=>[
                'lead_agregado_id'=>$lead_agregado_id
            ]
        ];
        try{
            $request = $this->client->request('POST','http://client.agentebrasil.com/client/lead_produto_agregado/venda_produto',[
                'verify'=>false,
                'form_params'=>$data
            ]);

            (new AytyLogsController())->updateLead([
                'lead_id'=>$lead_id,
                'lead_status'=>'auditado',
                'lead_fluxo'=>'02'
            ]);
            $resp = $request->getBody()->getContents();
            $obj['lead_dddtelefone'] = $objLead[0]['lead_fone'];
            $obj['lead_id'] = $objLead[0]['lead_id'];
            $obj['lead_nome'] = $objLead[0]['lead_nome'];
            $obj['lead_email'] = $objLead[0]['lead_email'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'status auditado',
                AytyLogs::formataLinhaLeadAytyLog($obj), json_encode($resp), $obj['lead_id'], '0.0.0.0'
            );
            $data =  ['return'=>200,"id"=>$lead_id,"pid"=>$pid];
            return $data;
        } catch (ClientException $e){
            $response = $e->getResponse();
            $req = $response->getBody()->getContents();
            $obj = [];
            $obj['lead_dddtelefone'] = $objLead[0]['lead_fone'];
            $obj['lead_id'] = $objLead[0]['lead_id'];
            $obj['lead_nome'] = $objLead[0]['lead_nome'];
            $obj['lead_email'] = $objLead[0]['lead_email'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'status auditado',
                AytyLogs::formataLinhaLeadAytyLog($obj), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), $obj['lead_id'], '0.0.0.0'
            );
            $data = ['return'=>500,"id"=>$lead_id,"pid"=>$pid];
            return $data;
        }
    }

    public function generateCob2($lead_id){
        //die("oi");
        $pid = AytyLogs::codProcess();
        $objLead = Lead::with('LeadAgregado')->where('lead_id',$lead_id)->get()->toArray();
        if(empty($objLead)) return ['return'=>500,'pid'=>$pid,'id'=>$lead_id,'action'=>'cobranca'];

        $lead_agregado_id = [];
        foreach ($objLead[0]['lead_agregado'] as $la){
            $lead_agregado_id[] = $la['lead_agregado_id'];
        }
        $object = json_decode($objLead[0]['lead_object_ayty']);
        $data = [
            'financeiro_contratacao'=>[
                'data_vencimento'=>$object->lead_vencimento_dia,
                'dia_recorrencia'=>$object->lead_recorrencia_dia,
                'financeiro_forma_pagamento_id'=>$object->lead_pagamento_forma == "boleto" ? 1 : 2
            ],
            'lead_agregado_produto'=>[
                'produto_id'=>$object->plan_id,
                'usuario_id'=>$object->come_id,
            ],
            'lead_agregado_produto_itens'=>[
                'lead_agregado_id'=>$lead_agregado_id
            ]
        ];
        //var_dump($data); exit;
        try{
            $request = $this->client->request('POST','http://client.agentebrasil.com/client/lead_produto_agregado2/venda_produto',[
                'verify'=>false,
                'form_params'=>$data
            ]);

            (new AytyLogsController())->updateLead([
                'lead_id'=>$lead_id,
                'lead_status'=>'auditado',
                'lead_fluxo'=>'02'
            ]);
            $resp = $request->getBody()->getContents();
            $obj['lead_dddtelefone'] = $objLead[0]['lead_fone'];
            $obj['lead_id'] = $objLead[0]['lead_id'];
            $obj['lead_nome'] = $objLead[0]['lead_nome'];
            $obj['lead_email'] = $objLead[0]['lead_email'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'status auditado',
                AytyLogs::formataLinhaLeadAytyLog($obj), json_encode($resp), $obj['lead_id'], '0.0.0.0'
            );
            $data =  ['return'=>200,"id"=>$lead_id,"pid"=>$pid];
            return $data;
        } catch (ClientException $e){
            $response = $e->getResponse();
            $req = $response->getBody()->getContents();
            $obj = [];
            $obj['lead_dddtelefone'] = $objLead[0]['lead_fone'];
            $obj['lead_id'] = $objLead[0]['lead_id'];
            $obj['lead_nome'] = $objLead[0]['lead_nome'];
            $obj['lead_email'] = $objLead[0]['lead_email'];
            AytyLogs::saveAytyLog(
                $pid,
                'ayty->ab', 'linha', 'status auditado',
                AytyLogs::formataLinhaLeadAytyLog($obj), json_encode([
                'message'=>$e->getMessage().PHP_EOL.$e->getLine().PHP_EOL.$e->getFile()
            ]), $obj['lead_id'], '0.0.0.0'
            );
            $data = ['return'=>500,"id"=>$lead_id,"pid"=>$pid];
            return $data;
        }
    }
}