<?php

namespace Modules\AytyProvider\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AytyProvider\Entities\AytyLogs;
use Modules\AytyProvider\Repositories\AytyLogsRepository;
use Modules\Client\Entities\Lead;


class AytyLogsController extends Controller
{
    protected $client;
    protected $baseUrl;
    protected $login;
    protected $password;

    public function __construct()
    {
        $this->client = new Client();
        $this->baseUrl = getenv('AYTY_BASE');
        $this->login = getenv('AYTY_LOGIN');
        $this->password = getenv('AYTY_PASS');
    }

    public $validator = [
        'new_lead'=>['lead_id', 'lead_nome', 'lead_dddtelefone', 'lead_email', 'lead_ip','fluxo','lead_status','lead_idcampaign'],
        'update_lead'=>['lead_id', 'fluxo', 'lead_status','lead_idcampaign'],
        'update_card'=>['lead_id','card_id','fluxo','lead_status'],
        'plan_update'=>['plan_id','plan_nome','plan_logica','plan_superlogica','plan_descricao','plan_valor'],
        'new_plan'=>['plan_nome','plan_status','plan_logica','plan_descricao','plan_parcelas','plan_valor']
    ];

    public function validar($data,$url,$execeptions)
    {
        $arrErro = [];
        if(count($data) > count($this->validator[$url])){
            $arrErro[] = "numero de parametros excede o permitido";
            return $arrErro;
        }
        foreach ($this->validator[$url] as $item):
            if($execeptions != null && in_array($item,$execeptions)){
                continue;
            }
            if(!isset($data[$item])){
                $arrErro[] = "campo ".$item." não passado";
            } elseif($data[$item]==""){
                $arrErro[] = "campo ".$item." está nullo";
            }
        endforeach;
        return $arrErro;
    }

    //login na ayty
    public function getLogin(){
        try {
            $credentials = json_encode(['username'=>$this->login,'password'=>$this->password]);
            //var_dump($credentials); exit;
            $r = $this->client->request('POST',$this->baseUrl."Login",[
                'headers'=>['Content-Type'=> 'application/json'],
                'body'=>$credentials
            ]);
            $token = json_decode($r->getBody()->getContents());
            //Gera Log
            AytyLogs::saveAytyLog($token->token,'ab->ayty','linear','login','',json_encode($token),'','0.0.0.0');
            return $token->token;
        } catch (\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/login_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            AytyLogs::saveAytyLog(AytyLogs::codProcess(),'ab->ayty','linear','login','',$e->getMessage(),'','0.0.0.0');
            return null;
        }
    }

    public function newLead($objectLead,$campanha=null)
    {
        $data = [
            'lead_id'=>$objectLead['lead_id'],
            'lead_nome'=>$objectLead['lead_nome'],
            'lead_dddtelefone'=>$objectLead['lead_fone'],
            'lead_email'=>$objectLead['lead_email'],
            'lead_status'=>'lead',
            'lead_ip'=>0,
        ];

        if(!is_null($data)) $data['lead_idcampaign'] = $campanha;

        $response = [];
        $valida = $this->validar($data,'new_lead',['lead_ip','lead_email','fluxo','lead_status','lead_idcampaign']);
        //var_dump($valida); exit;
        if(!empty($valida)){
            return response()->json(['erros'=>$valida],400);
        }
        //passa o token para completar a requisição
        $response['token']=$this->getLogin();
        try {
            $data['lead_status']='lead';
            $response['leads']=[$data];
            $body = json_encode($response);
            $r = $this->client->request('POST',$this->baseUrl."Lead/New",[
                'headers'=>['Content-Type'=> 'application/json'],
                'body'=>$body
            ]);
            $response = $r->getBody()->getContents();
            //var_dump($response); exit;
            $respJson = json_decode($response);
            $msg = [];
            if(!isset($respJson->count_error)) {
                $msg['msg'] = "lead_inserido_sucesso";
                $msg['status'] = "success";
                $msg['code'] = 200;
            } else {
                $msg['msg']="erro_ao_gravar_lead";
                $msg['status'] = "error";
                $msg['code'] = 400;
            }

            AytyLogs::saveAytyLog(
                $respJson->token,
                'ab->ayty','linha','inserir lead',
                AytyLogs::formataLinhaLeadAytyLog($data),json_encode($respJson),$data['lead_id'],$data['lead_ip']
            );
            file_put_contents(storage_path('logs/ayty_logs/new_lead_'.date('Y-m-d').'-'.time().".txt"),$response);
            Lead::where('lead_id',$data['lead_id'])->update([
                'lead_tag'=>'importado_ayty'
            ]);
            return response()->json(['message'=>$msg['msg'],'status'=>$msg['status']],$msg['code']);
        } catch (\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/new_lead_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            Lead::where('lead_id',$data['lead_id'])->update([
                'lead_tag'=>'erro_importacao_ayty'
            ]);
            return response()->json(['message'=>'error','status'=>'error'],400);
        }
    }

    public function updateLead($objectLead){
        $data = [
            'lead_id'=>$objectLead['lead_id'],
            'lead_status'=>$objectLead['lead_status'],
            'fluxo'=>$objectLead['lead_fluxo'],
        ];

        $response['token']=$this->getLogin();
        try {
            $data['lead_status']='lead';
            $response['leads']=[$data];
            $body = json_encode($response);
            $r = $this->client->request('POST',$this->baseUrl."Lead/Update",[
                'headers'=>['Content-Type'=> 'application/json'],
                'body'=>$body
            ]);
            $response = $r->getBody()->getContents();
            //var_dump($response); exit;
            $respJson = json_decode($response);
            $msg = [];
            if(!isset($respJson->count_error)) {
                $msg['msg'] = "lead_atualizado_sucesso";
                $msg['status'] = "success";
                $msg['code'] = 200;
            } else {
                $msg['msg']="erro_ao_atualizar_lead";
                $msg['status'] = "error";
                $msg['code'] = 400;
            }

            AytyLogs::saveAytyLog(
                $respJson->token,
                'ab->ayty','linha','inserir lead',
                AytyLogs::formataLinhaLeadAytyLog($data),json_encode($respJson),$data['lead_id'],$data['lead_ip']
            );
            file_put_contents(storage_path('logs/ayty_logs/update_lead_'.date('Y-m-d').'-'.time().".txt"),$response);
            Lead::where('lead_id',$data['lead_id'])->update([
                'lead_tag'=>'importado_ayty'
            ]);
            return response()->json(['message'=>$msg['msg'],'status'=>$msg['status']],$msg['code']);
        } catch (\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/update_lead_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            Lead::where('lead_id',$data['lead_id'])->update([
                'lead_tag'=>'erro_importacao_ayty'
            ]);
            return response()->json(['message'=>'error','status'=>'error'],400);
        }
    }
}