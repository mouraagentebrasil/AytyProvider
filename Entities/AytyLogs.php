<?php

namespace Modules\AytyProvider\Entities;

use Illuminate\Database\Eloquent\Model;

class AytyLogs extends Model
{
    protected $table='ayty_logs';
    protected $primaryKey='ayty_id';
    protected $fillable=['ayty_id','ayty_process','ayty_date','ayty_sentido','ayty_macro','ayty_fluxo','ayty_linha','ayty_json','ayty_retorno','lead_id','lead_ip'];
    public $timestamps = false;

    public function Lead()
    {
        return $this->belongsTo('\Modules\AytyProvider\Entities\Lead');
    }

    public static function saveAytyLog($token,$sentido,$macro,$fluxo,$linha,$obj_json,$lead_id,$ip)
    {
        $saveAytyLog = self::create([
            'ayty_process'=>$token,
            'ayty_date'=>date('Y-m-d H:i:s'),
            'ayty_sentido'=>$sentido,
            'ayty_macro'=>$macro,
            'ayty_fluxo'=>$fluxo,
            'ayty_linha'=>$linha,
            'ayty_json'=>$obj_json,
            'lead_id'=>$lead_id,
            'lead_ip'=>$ip,
        ]);
        return $saveAytyLog ? true: false;
    }

    public static function codProcess($length = 24)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function formataLinhaLeadAytyLog($data)
    {
        return $data['lead_id']."-".$data['lead_nome']."-".$data['lead_dddtelefone']."-".$data['lead_email'];
    }

    public static function formataLinhaLeadAgregadoAytyLog($data)
    {
        return $data['lead_id']."-".$data['titu_nome']."-".$data['titu_cpf'];
    }

    public static function formataLinhaLeadAgregadoBeneficiarioAytyLog($data)
    {
        return $data['lead_id']."-".$data['bene_nome']."-".$data['bene_cpf'];
    }

    public static function formataLinhaCardAytyLog($data)
    {
        return $data['plan_id']."-".$data['plan_nome']."-".$data['plan_parcelas']."-".$data['plan_valor']."-".$data['plan_superlogica'];
    }

    public static function formataLinhaPlanoAytyLog($data){
        return $data['plan_nome']."-".$data['plan_logica']."-".$data['plan_parcelas']
        ."-".$data['plan_valor']."-".$data['plan_superlogica'];
    }

    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
                continue;
            }
            if(!isset($data[$fillable[$i]])){
                $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
